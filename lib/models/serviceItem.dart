class ServiceItem {
  final String name;
  final int eco_price;
  final int pro_price;

  ServiceItem(this.name,this.eco_price,this.pro_price);
}