import 'package:flutter/material.dart';
import 'package:roadsideassistapp/screens/login.dart';
import 'package:roadsideassistapp/screens/quickhelp.dart';
import 'package:roadsideassistapp/screens/signup.dart';


void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      debugShowCheckedModeBanner: false,
      routes: {
        "login": (BuildContext context) => LoginScreen(),
        "signup": (BuildContext context) => Signup(),
        "quickhelp": (BuildContext context) => QuickHelp(),
      },
      home: Scaffold(
        backgroundColor: Colors.white,
        body: MyStatelessWidget(),
      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatelessWidget {
  MyStatelessWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 155.0,
                  child: Image.asset(
                    "assets/mekanic.png",
                    fit: BoxFit.contain,
                  ),
                ),
                Text(
                  "Get help in minutes with real time tracking and updates",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
                SizedBox(height: 25.0),
                MaterialButton(
                  height: 40.0,
                  minWidth: 200.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  color: Colors.lightBlue,
                  textColor: Colors.white,
                  child: new Text("Login"),
                  onPressed: () => {
                    Navigator.pushNamed(context,"login"),
                  },
                  splashColor: Colors.white,
                ),
                MaterialButton(
                  height: 40.0,
                  minWidth: 200.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  color: Colors.white,
                  textColor: Colors.red,
                  child: new Text("Signup"),
                  onPressed: () => {
                    Navigator.pushNamed(context,"signup"),
                  },
                  splashColor: Colors.redAccent,
                ),
                MaterialButton(
                  height: 40.0,
                  minWidth: 200.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  color: Colors.redAccent,
                  textColor: Colors.white,
                  child: new Text("Quick Help!"),
                  onPressed: () => {
                    Navigator.pushNamed(context,"quickhelp"),
                  },
                  splashColor: Colors.white,
                ),
              ],
            ),
          ),
        ));
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            // Navigate back to first route when tapped.
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}
