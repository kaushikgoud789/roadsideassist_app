import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:roadsideassistapp/models/serviceItem.dart';

final formatCurrency = new NumberFormat.simpleCurrency();
final List<ServiceItem> serviceItem = [
  ServiceItem("Battery", 55, 75),
  ServiceItem("Tire", 55, 75),
  ServiceItem("Tow", 55, 75),
  ServiceItem("Lockout", 55, 75),
  ServiceItem("Fuel", 55, 75)
];

int eco_price = 55;
int pro_price = 75;
String service_name = "Battery Service";
bool show = false;

class QuickHelp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    show = false;
    return _QuickHelpState();
  }
}

class _QuickHelpState extends State<QuickHelp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Name'),
      ),
      body: ServiceWidget(),
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.battery_alert),
                  color: Colors.blueAccent,
                  onPressed: () {
                    setState(() {
                      show = true;
                      eco_price = 55;
                      pro_price = 75;
                      service_name = "Battery Service";
                    });
                  },
                ),
                Text("Battery"),
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.donut_large),
                  color: Colors.blueAccent,
                  onPressed: () {
                    setState(() {
                      show = true;
                      eco_price = 55;
                      pro_price = 75;
                      service_name = "Tire Service";
                    });
                  },
                ),
                Text("Tire"),
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.build),
                  color: Colors.blueAccent,
                  onPressed: () {
                    setState(() {
                      show = true;
                      eco_price = 79;
                      pro_price = 99;
                      service_name = "Tow Service";
                    });
                  },
                ),
                Text("Tow"),
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.vpn_key),
                  color: Colors.blueAccent,
                  onPressed: () {
                    setState(() {
                      show = true;
                      eco_price = 45;
                      pro_price = 65;
                      service_name = "Lockout Service";
                    });
                  },
                ),
                Text("Lockout"),
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.signal_cellular_connected_no_internet_4_bar),
                  color: Colors.blueAccent,
                  onPressed: () {
                    setState(() {
                      show = true;
                      eco_price = 50;
                      pro_price = 50;
                      service_name = "Fuel Service";
                    });
                  },
                ),
                Text("Fuel"),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

//class ServiceCostWidget extends StatefulWidget {
//  final String serviceName;
//  const ServiceCostWidget(this.serviceName);
//  @override
//  ServiceCostWidgetState createState() => ServiceCostWidgetState();
//}
//
//class ServiceCostWidgetState extends State<ServiceCostWidget> {
//  @override
//  Widget build(BuildContext context) {
//    // Here you direct access using widget
//    return Text(widget.serviceName);
//  }
//}

class ServiceWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Flexible(
          child: GridView.count(
            primary: false,
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(4),
                child: const Text("Battery"),
                color: Colors.blue[100],
                alignment: Alignment(0.0, 0.0),
              ),
              Container(
                padding: const EdgeInsets.all(4),
                child: const Text('Flat Tire'),
                color: Colors.blue[100],
                alignment: Alignment(0.0, 0.0),
              ),
              Container(
                padding: const EdgeInsets.all(4),
                child: const Text('Towing'),
                color: Colors.blue[100],
                alignment: Alignment(0.0, 0.0),
              ),
              Container(
                padding: const EdgeInsets.all(4),
                child: const Text('Lockout'),
                color: Colors.blue[100],
                alignment: Alignment(0.0, 0.0),
              ),
              Container(
                padding: const EdgeInsets.all(4),
                child: const Text('Fuel'),
                color: Colors.blue[100],
                alignment: Alignment(0.0, 0.0),
              ),
              Container(
                padding: const EdgeInsets.all(4),
                child: const Text('Other'),
                color: Colors.blue[100],
                alignment: Alignment(0.0, 0.0),
              ),
            ],
          )),
      if (show)
        Padding(
            padding: const EdgeInsets.all(20),
            child: Text(
              '$service_name',
              textAlign: TextAlign.center,
              style: DefaultTextStyle.of(context).style.apply(fontSizeFactor: 1.4),
            )),
      if (show)
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            InkWell(
              child: Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width / 3,
                margin: const EdgeInsets.only(right: 15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  border: Border.all(
                    color: Colors.black,
                    width: 2,
                  ),
                  color: Colors.white,
                ),
                padding: const EdgeInsets.all(4),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Economy',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.green.withOpacity(1.0)),
                      ),
                      Text(
                        '${formatCurrency.format(eco_price)}',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.red.withOpacity(1.0), fontSize: 25),
                      ),
                    ]),
              ),
              onTap: () {
                print("Economy was tapped");
              },
            ),
            InkWell(
              child: Container(
                padding: const EdgeInsets.all(4),
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width / 3,
                margin: const EdgeInsets.only(right: 15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  border: Border.all(
                    color: Colors.black,
                    width: 2,
                  ),
                  color: Colors.white,
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Professional',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.green.withOpacity(1.0)),
                      ),
                      Text(
                        '${formatCurrency.format(pro_price)}',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.red.withOpacity(1.0), fontSize: 25),
                      ),
                    ]),
              ),
              onTap: () {
                print("Professional was tapped");
              },
            ),
          ],
        ),
    ]);
//    return Center(
//      child: RaisedButton(
//        onPressed: () {
//          // Navigate back to first route when tapped.
//          Navigator.pop(context);
//        },
//        child: Text('Back'),
//      ),
//    );
  }
}
